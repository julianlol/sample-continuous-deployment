FROM debian:jessie

MAINTAINER Anthony K GROSS<anthony.k.gross@gmail.com>

WORKDIR /src

RUN apt-get update -y && \
	apt-get upgrade -y && \
	apt-get install -y wget && \
	apt-get install -y php5-common php5-cli php5-fpm php5-mcrypt \
	                    php5-mysql php5-apcu php5-gd php5-imagick \
	                    php5-curl php5-intl php5-sqlite && \
	apt-get install -y curl git && \
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    chmod a+x composer.phar && \
    mv composer.phar /usr/local/bin/composer && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get autoremove -y --purge && \
    usermod -u 1000 www-data

RUN apt-get update -y && \
	apt-get upgrade -y && \
	apt-get install -y supervisor nginx && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get autoremove -y --purge && \
    usermod -u 1000 www-data

COPY entrypoint.sh /entrypoint.sh
COPY conf/supervisor /etc/supervisor/conf.d
COPY conf/nginx /etc/nginx
COPY src /src

RUN chmod 777 /src -Rf && \
    mkdir -p /logs && \
    chmod 777 /logs -Rf && \
    chmod +x /entrypoint.sh && \
    sh /entrypoint.sh install && \
    rm web/app_dev.php

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/entrypoint.sh"]
CMD ["run"]